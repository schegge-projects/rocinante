package de.schegge.rosinante.utilities;

import de.schegge.rosinante.parser.ParseException;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ProtoSourceParameterResolver implements ParameterResolver {
    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == StringProtoSources.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        StringProtoSources sources = new StringProtoSources();
        Optional<FileSource> annotation = parameterContext.findAnnotation(FileSource.class);
        if (annotation.isPresent()) {
            annotation.ifPresent(x -> handleFileSource(parameterContext.getDeclaringExecutable().getDeclaringClass(), x, sources));
        } else {
            parameterContext.findAnnotation(StringSource.class).ifPresent(x -> handleStringSource(x, sources));
        }
        return sources;
    }

    private void handleStringSource(StringSource annotation, StringProtoSources sources) {
        String content = Stream.of(annotation.content(), annotation.value()).filter(Objects::nonNull)
                .filter(Predicate.not(String::isEmpty)).findFirst().orElse("");
        sources.addSource(annotation.name(), content);
    }

    private static void handleFileSource(Class<?> type, FileSource source, StringProtoSources sources) {
        String input = Stream.of(source.input(), source.value()).filter(Objects::nonNull)
                .filter(Predicate.not(String::isEmpty)).findFirst().orElseThrow(() -> new ParseException("no input defined"));
        getAddSource(type, sources, input, input);
        Stream.of(source.imports()).forEach(i -> getAddSource(type, sources, i, "import/" + i));
    }

    private static void getAddSource(Class<?> type, StringProtoSources sources, String name, String path) {
        try {
            sources.addSource(path, Path.of(type.getClassLoader().getResource(name).toURI()));
        } catch (URISyntaxException | IOException e) {
            throw new ParameterResolutionException(e.toString());
        }
    }
}
