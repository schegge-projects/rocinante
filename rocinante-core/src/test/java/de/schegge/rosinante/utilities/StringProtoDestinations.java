package de.schegge.rosinante.utilities;

import de.schegge.rosinante.core.ProtoDestinations;

import java.util.HashMap;
import java.util.Map;

public class StringProtoDestinations implements ProtoDestinations {
    private final Map<String, String> files = new HashMap<>();

    @Override
    public void writeFile(String packageName, String fileName, String content) {
        files.put(fileName, content);
    }

    public String getContent(String fileName) {
        return files.get(fileName);
    }
}
