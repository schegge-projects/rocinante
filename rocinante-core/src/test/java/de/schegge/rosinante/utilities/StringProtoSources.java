package de.schegge.rosinante.utilities;

import de.schegge.rosinante.core.ProtoSources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class StringProtoSources implements ProtoSources {
    private final Map<String, String> sources = new HashMap<>();

    public void addSource(String name, String content) {
        sources.put(name, content);
    }

    public void addSource(String name, Path path) throws IOException {
        sources.put(name, Files.readString(path));
    }

    @Override
    public String getContent(String name) {
        return sources.get(name);
    }

    @Override
    public String getImport(String name) {
        return sources.get("import/" + name);
    }
}
