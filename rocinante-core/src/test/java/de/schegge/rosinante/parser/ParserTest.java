package de.schegge.rosinante.parser;

import de.schegge.rosinante.core.ProtoVersion;
import de.schegge.rosinante.parser.ast.SyntaxProduction;
import de.schegge.rosinante.utilities.FileSource;
import de.schegge.rosinante.utilities.ProtoSourceParameterResolver;
import de.schegge.rosinante.utilities.StringProtoSources;
import de.schegge.rosinante.utilities.StringSource;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(ProtoSourceParameterResolver.class)
class ParserTest {
    @ParameterizedTest
    @CsvSource(value = {
            "syntax = \"proto3\";|PROTO3",
            "syntax = \"proto2\";|PROTO2"
    }, delimiter = '|')
    void parseSyntax(String input, ProtoVersion expected, StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", input);
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        SyntaxProduction syntaxProduction = parser.rootNode().firstChildOfType(SyntaxProduction.class);
        String syntax = syntaxProduction.get(2).toString().toUpperCase();
        assertEquals(expected, ProtoVersion.valueOf(syntax.substring(1, syntax.length() - 1)));
    }

    @Test
    @Tag("rosi")
    @Disabled
    void parseRosiSyntax(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", "syntax = \"rosi1\";");
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        SyntaxProduction syntaxProduction = parser.rootNode().firstChildOfType(SyntaxProduction.class);
        String syntax = syntaxProduction.get(2).toString().toUpperCase();
        assertEquals(ProtoVersion.ROSI1, ProtoVersion.valueOf(syntax.substring(1, syntax.length() - 1)));
    }

    @Test
    void parseExample(@FileSource(value = "test1.proto", imports = "other.proto") StringProtoSources sources) throws IOException {
        ProtocolbuffersParser parser = new ProtocolbuffersParser("test1.proto", sources);
        parser.Proto();
        assertNotNull(parser.rootNode());
    }

    @Test
    void parseExampleWithDuplicatedTypeName(@FileSource("exampleWithDuplicatedTypeName.proto") StringProtoSources sources) throws IOException {
        ProtocolbuffersParser parser = new ProtocolbuffersParser("exampleWithDuplicatedTypeName.proto", sources);
        ParseException parseException = assertThrows(ParseException.class, parser::Proto);
        assertEquals("duplicate type name: de.schegge.example.Example", parseException.getMessage());
    }

    @Test
    void parseInvalidSyntax(@StringSource(content="syntax = \"proton3\";") StringProtoSources sources) throws IOException {
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        ParseException parseException = assertThrows(ParseException.class, parser::Proto);
        assertEquals("""
                
                Encountered an error at (or somewhere around) input.proto:1:10
                Found string "\\"proton3\\"" of type STRING_LITERAL""", parseException.getMessage());
    }

    @Test
    void parseSyntaxWithPackage(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                package test;
                """);
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        assertNotNull(parser.rootNode());
    }

    @Test
    void parseSyntaxWithOption(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                
                option java_package = "com.example.foo";
                """);
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        assertNotNull(parser.rootNode());
    }

    @Test
    void parseMessageWithOption(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                
                message Test {
                  string text = 1;
                  int number = 2 [deprecated = true];
                };
                """);
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        assertNotNull(parser.rootNode());
    }

    @Test
    void parseRepeatedField(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                
                message Test {
                  string text = 1;
                  repeated int number = 2;
                };
                """);
        ProtocolbuffersParser parser = new ProtocolbuffersParser("input.proto", sources);
        parser.Proto();
        assertNotNull(parser.rootNode());
    }
}

