package de.schegge.rosinante.generator;

import de.schegge.rosinante.core.PathProtoDestinations;
import de.schegge.rosinante.utilities.FileSource;
import de.schegge.rosinante.utilities.ProtoSourceParameterResolver;
import de.schegge.rosinante.utilities.StringProtoDestinations;
import de.schegge.rosinante.utilities.StringProtoSources;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ProtoSourceParameterResolver.class)
class ProtoGeneratorTest {

    @Test
    void generate(@FileSource("example.proto") StringProtoSources sources) throws IOException {
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("example.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;
                
                import java.util.Optional;
                import java.util.List;

                public record Example(String text, boolean flag, int number) {
                  public Optional<String> getText() {
                    return Optional.ofNullable(text());
                  }
                }
                """, destinations.getContent("Example.java"));
        assertEquals("""
                package de.schegge.example;

                import de.schegge.rosinante.io.Builder;
                import de.schegge.rosinante.io.LimitedProtoInputStream;
                import de.schegge.rosinante.io.Proto;
                import de.schegge.rosinante.io.ProtoInputStream;
                import de.schegge.rosinante.io.ProtoOutputStream;
                import de.schegge.rosinante.io.WireType;

                import java.io.ByteArrayOutputStream;
                import java.io.IOException;
                import java.io.InputStream;
                import java.io.OutputStream;
                import java.util.Objects;
                import java.util.Optional;
                import java.util.BitSet;
                import java.util.List;
                import java.util.ArrayList;
                
                public final class ExampleProto implements Proto<Example> {
                      public static class ExampleBuilder implements Builder<Example> {
                          private String text = null;
                          private boolean flag = false;
                          private int number = 0;
                          BitSet initialized = new BitSet();

                          public ExampleBuilder setText(String text) {
                              this.text = Objects.requireNonNull(text);
                              return this;
                          }
                          public ExampleBuilder setFlag(boolean flag) {
                              this.flag = flag;
                              initialized.set(2);
                              return this;
                          }
                          public ExampleBuilder setNumber(int number) {
                              this.number = number;
                              initialized.set(3);
                              return this;
                          }

                          @Override
                          public Example build() {
                              if (initialized.cardinality() != 2) {
                                  throw new IllegalArgumentException("some fields are not initialized");
                              }
                              return new Example(text, flag, number);
                          }
                      }

                      @Override
                      public ExampleBuilder builder() {
                          return new ExampleBuilder();
                      }

                      @Override
                      public Example read(InputStream inputStream) throws IOException {
                          ProtoInputStream protoInputStream = new ProtoInputStream(inputStream);
                          String text = null;
                          boolean flag = false;
                          int number = 0;
                          BitSet initialized = new BitSet();
                
                          WireType wireType;
                          while ((wireType = protoInputStream.readType()) != null) {
                              int lastFieldNumber = protoInputStream.getLastFieldNumber();
                              switch (lastFieldNumber) {
                                  case 1:
                                      text = protoInputStream.readString(wireType);
                                      break;
                                  case 2:
                                      flag = protoInputStream.readBoolean(wireType);
                                      initialized.set(2);
                                      break;
                                  case 3:
                                      number = protoInputStream.readInteger(wireType);
                                      initialized.set(3);
                                      break;
                                  default:
                                      throw new IOException("invalid field number: " + lastFieldNumber);
                              }
                          }
                          if (initialized.cardinality() != 2) {
                              throw new IOException("some fields are not initialized");
                          }
                          return new Example(text, flag, number);
                      }

                      @Override
                      public void write(OutputStream outputStream, Example value) throws IOException {
                          ProtoOutputStream protoOutputStream = new ProtoOutputStream(outputStream);
                          if (value.getText().isPresent()) {
                            protoOutputStream.writeString(1, value.getText().get());
                          }
                          protoOutputStream.writeBoolean(2, value.flag());
                          protoOutputStream.writeInteger(3, value.number());
                      }
                }
                """, destinations.getContent("ExampleProto.java"));
    }

    @Test
    void generateToFile(@FileSource("example.proto") StringProtoSources sources, @TempDir Path destination) throws IOException {
        PathProtoDestinations destinations = new PathProtoDestinations(destination);
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("example.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;
                
                import java.util.Optional;
                import java.util.List;
                
                public record Example(String text, boolean flag, int number) {
                  public Optional<String> getText() {
                    return Optional.ofNullable(text());
                  }
                }
                """, Files.readString(destination.resolve("de/schegge/example/Example.java")));
        assertEquals("""
                package de.schegge.example;

                import de.schegge.rosinante.io.Builder;
                import de.schegge.rosinante.io.LimitedProtoInputStream;
                import de.schegge.rosinante.io.Proto;
                import de.schegge.rosinante.io.ProtoInputStream;
                import de.schegge.rosinante.io.ProtoOutputStream;
                import de.schegge.rosinante.io.WireType;

                import java.io.ByteArrayOutputStream;
                import java.io.IOException;
                import java.io.InputStream;
                import java.io.OutputStream;
                import java.util.Objects;
                import java.util.Optional;
                import java.util.BitSet;
                import java.util.List;
                import java.util.ArrayList;
                
                public final class ExampleProto implements Proto<Example> {
                      public static class ExampleBuilder implements Builder<Example> {
                          private String text = null;
                          private boolean flag = false;
                          private int number = 0;
                          BitSet initialized = new BitSet();

                          public ExampleBuilder setText(String text) {
                              this.text = Objects.requireNonNull(text);
                              return this;
                          }
                          public ExampleBuilder setFlag(boolean flag) {
                              this.flag = flag;
                              initialized.set(2);
                              return this;
                          }
                          public ExampleBuilder setNumber(int number) {
                              this.number = number;
                              initialized.set(3);
                              return this;
                          }

                          @Override
                          public Example build() {
                              if (initialized.cardinality() != 2) {
                                  throw new IllegalArgumentException("some fields are not initialized");
                              }
                              return new Example(text, flag, number);
                          }
                      }

                      @Override
                      public ExampleBuilder builder() {
                          return new ExampleBuilder();
                      }

                      @Override
                      public Example read(InputStream inputStream) throws IOException {
                          ProtoInputStream protoInputStream = new ProtoInputStream(inputStream);
                          String text = null;
                          boolean flag = false;
                          int number = 0;
                          BitSet initialized = new BitSet();
               
                          WireType wireType;
                          while ((wireType = protoInputStream.readType()) != null) {
                              int lastFieldNumber = protoInputStream.getLastFieldNumber();
                              switch (lastFieldNumber) {
                                  case 1:
                                      text = protoInputStream.readString(wireType);
                                      break;
                                  case 2:
                                      flag = protoInputStream.readBoolean(wireType);
                                      initialized.set(2);
                                      break;
                                  case 3:
                                      number = protoInputStream.readInteger(wireType);
                                      initialized.set(3);
                                      break;
                                  default:
                                      throw new IOException("invalid field number: " + lastFieldNumber);
                              }
                          }
                          if (initialized.cardinality() != 2) {
                              throw new IOException("some fields are not initialized");
                          }
                          return new Example(text, flag, number);
                      }

                      @Override
                      public void write(OutputStream outputStream, Example value) throws IOException {
                          ProtoOutputStream protoOutputStream = new ProtoOutputStream(outputStream);
                          if (value.getText().isPresent()) {
                            protoOutputStream.writeString(1, value.getText().get());
                          }
                          protoOutputStream.writeBoolean(2, value.flag());
                          protoOutputStream.writeInteger(3, value.number());
                      }
                }
                """, Files.readString(destination.resolve("de/schegge/example/ExampleProto.java")));
    }

    @Test
    void generateAllOptionalFields(@FileSource("exampleAllOptional.proto") StringProtoSources sources) throws IOException {
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("exampleAllOptional.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;
                
                import java.util.Optional;
                import java.util.List;

                public record Example(String text, Boolean flag, Integer number) {
                  public Optional<String> getText() {
                    return Optional.ofNullable(text());
                  }
                  public Optional<Boolean> getFlag() {
                    return Optional.ofNullable(flag());
                  }
                  public Optional<Integer> getNumber() {
                    return Optional.ofNullable(number());
                  }
                }
                """, destinations.getContent("Example.java"));
        assertEquals("""
                package de.schegge.example;

                import de.schegge.rosinante.io.Builder;
                import de.schegge.rosinante.io.LimitedProtoInputStream;
                import de.schegge.rosinante.io.Proto;
                import de.schegge.rosinante.io.ProtoInputStream;
                import de.schegge.rosinante.io.ProtoOutputStream;
                import de.schegge.rosinante.io.WireType;

                import java.io.ByteArrayOutputStream;
                import java.io.IOException;
                import java.io.InputStream;
                import java.io.OutputStream;
                import java.util.Objects;
                import java.util.Optional;
                import java.util.BitSet;
                import java.util.List;
                import java.util.ArrayList;
                
                public final class ExampleProto implements Proto<Example> {
                      public static class ExampleBuilder implements Builder<Example> {
                          private String text = null;
                          private Boolean flag = null;
                          private Integer number = null;

                          public ExampleBuilder setText(String text) {
                              this.text = Objects.requireNonNull(text);
                              return this;
                          }
                          public ExampleBuilder setFlag(Boolean flag) {
                              this.flag = Objects.requireNonNull(flag);
                              return this;
                          }
                          public ExampleBuilder setNumber(Integer number) {
                              this.number = Objects.requireNonNull(number);
                              return this;
                          }

                          @Override
                          public Example build() {
                              return new Example(text, flag, number);
                          }
                      }

                      @Override
                      public ExampleBuilder builder() {
                          return new ExampleBuilder();
                      }

                      @Override
                      public Example read(InputStream inputStream) throws IOException {
                          ProtoInputStream protoInputStream = new ProtoInputStream(inputStream);
                          String text = null;
                          Boolean flag = null;
                          Integer number = null;
                
                          WireType wireType;
                          while ((wireType = protoInputStream.readType()) != null) {
                              int lastFieldNumber = protoInputStream.getLastFieldNumber();
                              switch (lastFieldNumber) {
                                  case 1:
                                      text = protoInputStream.readString(wireType);
                                      break;
                                  case 2:
                                      flag = protoInputStream.readBoolean(wireType);
                                      break;
                                  case 3:
                                      number = protoInputStream.readInteger(wireType);
                                      break;
                                  default:
                                      throw new IOException("invalid field number: " + lastFieldNumber);
                              }
                          }
                          return new Example(text, flag, number);
                      }

                      @Override
                      public void write(OutputStream outputStream, Example value) throws IOException {
                          ProtoOutputStream protoOutputStream = new ProtoOutputStream(outputStream);
                          if (value.getText().isPresent()) {
                            protoOutputStream.writeString(1, value.getText().get());
                          }
                          if (value.getFlag().isPresent()) {
                            protoOutputStream.writeBoolean(2, value.getFlag().get());
                          }
                          if (value.getNumber().isPresent()) {
                            protoOutputStream.writeInteger(3, value.getNumber().get());
                          }
                      }
                }
                """, destinations.getContent("ExampleProto.java"));
    }

    @Test
    void generateWithEnum(@FileSource("proto3/exampleWithEnum.proto") StringProtoSources sources) throws IOException {
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("proto3/exampleWithEnum.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;
                
                import java.util.Optional;
                import java.util.List;

                public record Example(String text, boolean flag, int number, EnumAllowingAlias eea) {
                  public Optional<String> getText() {
                    return Optional.ofNullable(text());
                  }
                }
                """, destinations.getContent("Example.java"));
        assertEquals("""
                package de.schegge.example;

                import java.util.Optional;

                public enum EnumAllowingAlias {
                  EAA_UNSPECIFIED,
                  EAA_STARTED,
                  EAA_RUNNING,
                  EAA_FINISHED;
                
                  public static EnumAllowingAlias byFieldNumber(int fieldNumber) {
                    return switch (fieldNumber) {
                      case 0 -> EAA_UNSPECIFIED;
                      case 1 -> EAA_STARTED;
                      case 2 -> EAA_FINISHED;
                      default -> throw new IllegalArgumentException("invalid field number for EnumAllowingAlias: " + fieldNumber);
                    };
                  }

                  public static int getFieldNumber(EnumAllowingAlias value) {
                    return switch (value) {
                      case EAA_UNSPECIFIED -> 0;
                      case EAA_STARTED, EAA_RUNNING -> 1;
                      case EAA_FINISHED -> 2;
                    };
                  }
                }
                """, destinations.getContent("EnumAllowingAlias.java"));
       assertEquals("""
                package de.schegge.example;

                import de.schegge.rosinante.io.Builder;
                import de.schegge.rosinante.io.LimitedProtoInputStream;
                import de.schegge.rosinante.io.Proto;
                import de.schegge.rosinante.io.ProtoInputStream;
                import de.schegge.rosinante.io.ProtoOutputStream;
                import de.schegge.rosinante.io.WireType;

                import java.io.ByteArrayOutputStream;
                import java.io.IOException;
                import java.io.InputStream;
                import java.io.OutputStream;
                import java.util.Objects;
                import java.util.Optional;
                import java.util.BitSet;
                import java.util.List;
                import java.util.ArrayList;

                public final class ExampleProto implements Proto<Example> {
                      public static class ExampleBuilder implements Builder<Example> {
                          private String text = null;
                          private boolean flag = false;
                          private int number = 0;
                          private EnumAllowingAlias eea = null;
                          BitSet initialized = new BitSet();

                          public ExampleBuilder setText(String text) {
                              this.text = Objects.requireNonNull(text);
                              return this;
                          }
                          public ExampleBuilder setFlag(boolean flag) {
                              this.flag = flag;
                              initialized.set(2);
                              return this;
                          }
                          public ExampleBuilder setNumber(int number) {
                              this.number = number;
                              initialized.set(3);
                              return this;
                          }
                          public ExampleBuilder setEea(EnumAllowingAlias eea) {
                              this.eea = Objects.requireNonNull(eea);
                              initialized.set(4);
                              return this;
                          }

                          @Override
                          public Example build() {
                              if (initialized.cardinality() != 3) {
                                  throw new IllegalArgumentException("some fields are not initialized");
                              }
                              return new Example(text, flag, number, eea);
                          }
                      }

                      @Override
                      public ExampleBuilder builder() {
                          return new ExampleBuilder();
                      }

                      @Override
                      public Example read(InputStream inputStream) throws IOException {
                          ProtoInputStream protoInputStream = new ProtoInputStream(inputStream);
                          String text = null;
                          boolean flag = false;
                          int number = 0;
                          EnumAllowingAlias eea = null;
                          BitSet initialized = new BitSet();
                
                          WireType wireType;
                          while ((wireType = protoInputStream.readType()) != null) {
                              int lastFieldNumber = protoInputStream.getLastFieldNumber();
                              switch (lastFieldNumber) {
                                  case 1:
                                      text = protoInputStream.readString(wireType);
                                      break;
                                  case 2:
                                      flag = protoInputStream.readBoolean(wireType);
                                      initialized.set(2);
                                      break;
                                  case 3:
                                      number = protoInputStream.readInteger(wireType);
                                      initialized.set(3);
                                      break;
                                  case 4:
                                      int index = protoInputStream.readInteger(wireType);
                                      eea = EnumAllowingAlias.byFieldNumber(index);
                                      initialized.set(4);
                                      break;
                                  default:
                                      throw new IOException("invalid field number: " + lastFieldNumber);
                              }
                          }
                          if (initialized.cardinality() != 3) {
                              throw new IOException("some fields are not initialized");
                          }
                          return new Example(text, flag, number, eea);
                      }

                      @Override
                      public void write(OutputStream outputStream, Example value) throws IOException {
                          ProtoOutputStream protoOutputStream = new ProtoOutputStream(outputStream);
                          if (value.getText().isPresent()) {
                            protoOutputStream.writeString(1, value.getText().get());
                          }
                          protoOutputStream.writeBoolean(2, value.flag());
                          protoOutputStream.writeInteger(3, value.number());
                          protoOutputStream.writeInteger(4, EnumAllowingAlias.getFieldNumber(value.eea()));
                      }
                }
                """, destinations.getContent("ExampleProto.java"));
    }

    @Test
    @Tag("proto3")
    void generateEnumWithOption(@FileSource("proto3/exampleEnumWithOption.proto") StringProtoSources sources) throws IOException {
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("proto3/exampleEnumWithOption.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;

                import java.util.Optional;

                public enum Example {
                  EAA_UNSPECIFIED,
                  EAA_STARTED,
                  EAA_RUNNING,
                  EAA_FINISHED;
                
                  public static Example byFieldNumber(int fieldNumber) {
                    return switch (fieldNumber) {
                      case 0 -> EAA_UNSPECIFIED;
                      case 1 -> EAA_STARTED;
                      case 2 -> EAA_FINISHED;
                      default -> throw new IllegalArgumentException("invalid field number for Example: " + fieldNumber);
                    };
                  }

                  public static int getFieldNumber(Example value) {
                    return switch (value) {
                      case EAA_UNSPECIFIED -> 0;
                      case EAA_STARTED, EAA_RUNNING -> 1;
                      case EAA_FINISHED -> 2;
                    };
                  }
                }
                """, destinations.getContent("Example.java"));
    }

    @Test
    @Tag("proto3")
    void generateEnumWithoutAliasButWithOption(@FileSource("proto3/exampleEnumWithoutAliasButWithOption.proto") StringProtoSources sources) throws IOException {
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("proto3/exampleEnumWithoutAliasButWithOption.proto", sources, destinations);
        assertEquals("""
                package de.schegge.example;

                import java.util.Optional;

                public enum Example {
                  EAA_UNSPECIFIED,
                  EAA_STARTED,
                  EAA_FINISHED;
                
                  public static Example byFieldNumber(int fieldNumber) {
                    return switch (fieldNumber) {
                      case 0 -> EAA_UNSPECIFIED;
                      case 1 -> EAA_STARTED;
                      case 2 -> EAA_FINISHED;
                      default -> throw new IllegalArgumentException("invalid field number for Example: " + fieldNumber);
                    };
                  }

                  public static int getFieldNumber(Example value) {
                    return switch (value) {
                      case EAA_UNSPECIFIED -> 0;
                      case EAA_STARTED -> 1;
                      case EAA_FINISHED -> 2;
                    };
                  }
                }
                """, destinations.getContent("Example.java"));
    }

    @Test
    void messageWithFieldOption(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                
                message Test {
                  string text = 1;
                  int32 number = 2 [deprecated = true];
                };
                """);
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("input.proto", sources, destinations);
        assertEquals("""
                import java.util.Optional;
                import java.util.List;
                
                public record Test(String text, int number) {
                }
                """, destinations.getContent("Test.java"));
    }

    @Test
    void messageWithRepeatedField(StringProtoSources sources) throws IOException {
        sources.addSource("input.proto", """
                syntax = "proto3";
                
                message Test {
                  string text = 1;
                  repeated int32 number = 2;
                };
                """);
        StringProtoDestinations destinations = new StringProtoDestinations();
        ProtoGenerator protoGenerator = new ProtoGenerator();
        protoGenerator.generate("input.proto", sources, destinations);
        assertEquals("""
                import java.util.Optional;
                import java.util.List;
                
                public record Test(String text, List<Integer> number) {
                }
                """, destinations.getContent("Test.java"));
        assertEquals("""
                import de.schegge.rosinante.io.Builder;
                import de.schegge.rosinante.io.LimitedProtoInputStream;
                import de.schegge.rosinante.io.Proto;
                import de.schegge.rosinante.io.ProtoInputStream;
                import de.schegge.rosinante.io.ProtoOutputStream;
                import de.schegge.rosinante.io.WireType;

                import java.io.ByteArrayOutputStream;
                import java.io.IOException;
                import java.io.InputStream;
                import java.io.OutputStream;
                import java.util.Objects;
                import java.util.Optional;
                import java.util.BitSet;
                import java.util.List;
                import java.util.ArrayList;

                public final class TestProto implements Proto<Test> {
                      public static class TestBuilder implements Builder<Test> {
                          private String text = "";
                          private List<Integer> number = new ArrayList<>();
                          BitSet initialized = new BitSet();

                          public TestBuilder setText(String text) {
                              this.text = Objects.requireNonNull(text);
                              initialized.set(1);
                              return this;
                          }
                          public TestBuilder setNumber(List<Integer> number) {
                              this.number = Objects.requireNonNull(number);
                              initialized.set(2);
                              return this;
                          }

                          @Override
                          public Test build() {
                              if (initialized.cardinality() != 2) {
                                  throw new IllegalArgumentException("some fields are not initialized");
                              }
                              return new Test(text, number);
                          }
                      }

                      @Override
                      public TestBuilder builder() {
                          return new TestBuilder();
                      }

                      @Override
                      public Test read(InputStream inputStream) throws IOException {
                          ProtoInputStream protoInputStream = new ProtoInputStream(inputStream);
                          String text = "";
                          List<Integer> number = new ArrayList<>();
                          BitSet initialized = new BitSet();

                          WireType wireType;
                          while ((wireType = protoInputStream.readType()) != null) {
                              int lastFieldNumber = protoInputStream.getLastFieldNumber();
                              switch (lastFieldNumber) {
                                  case 1:
                                      text = protoInputStream.readString(wireType);
                                      initialized.set(1);
                                      break;
                                  case 2:
                                      number.addAll(protoInputStream.readIntegers(wireType));
                                      initialized.set(2);
                                      break;
                                  default:
                                      throw new IOException("invalid field number: " + lastFieldNumber);
                              }
                          }
                          if (initialized.cardinality() != 2) {
                              throw new IOException("some fields are not initialized");
                          }
                          return new Test(text, number);
                      }

                      @Override
                      public void write(OutputStream outputStream, Test value) throws IOException {
                          ProtoOutputStream protoOutputStream = new ProtoOutputStream(outputStream);
                          protoOutputStream.writeString(1, value.text());
                          protoOutputStream.writeInteger(2, value.number());
                      }
                }
                """, destinations.getContent("TestProto.java"));
    }
}