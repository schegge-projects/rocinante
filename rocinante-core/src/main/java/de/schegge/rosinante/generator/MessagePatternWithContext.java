package de.schegge.rosinante.generator;

public record MessagePatternWithContext(MessagePattern pattern, GeneratorContext context) {
}
