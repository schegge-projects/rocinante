package de.schegge.rosinante.generator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnumPattern {
    private final String name;
    private final Map<Integer, EnumFieldPattern> fields = new HashMap<>();

    private boolean allowAlias;

    public EnumPattern(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<EnumFieldPattern> getFields() {
        return List.copyOf(fields.values());
    }

    public void addField(EnumFieldPattern pattern) {
        checkForDuplicates(pattern);
        EnumFieldPattern fieldPattern = fields.get(pattern.fieldNumber());
        if (fieldPattern != null) {
            fieldPattern.names().addAll(pattern.names());
        } else {
            fields.put(pattern.fieldNumber(), pattern);
        }
    }

    private void checkForDuplicates(EnumFieldPattern pattern) {
        List<String> existingFieldNames = fields.values().stream().map(EnumFieldPattern::names)
                .flatMap(List::stream).filter(x -> pattern.names().contains(x)).toList();
        if (!existingFieldNames.isEmpty()) {
            throw new IllegalArgumentException("duplicate field name: " + existingFieldNames);
        }
    }

    public void setAllowAlias(Boolean allowAlias) {
        this.allowAlias = allowAlias;
    }

    public boolean isAllowAlias() {
        return allowAlias;
    }
}
