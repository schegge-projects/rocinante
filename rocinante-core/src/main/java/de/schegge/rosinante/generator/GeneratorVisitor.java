package de.schegge.rosinante.generator;

import de.schegge.rosinante.parser.ast.EnumProduction;
import de.schegge.rosinante.parser.ast.ImportProduction;
import de.schegge.rosinante.parser.ast.MessageProduction;
import de.schegge.rosinante.parser.ast.PackageProduction;
import de.schegge.rosinante.parser.ast.Proto;

class GeneratorVisitor implements ProtoVisitor<GeneratorContext, Void> {

    private static final EnumVisitor ENUM_VISITOR = new EnumVisitor();
    private static final MessageVisitor MESSAGE_VISITOR = new MessageVisitor();

    @Override
    public Void visit(Proto proto, GeneratorContext context) {
        proto.childrenOfType(PackageProduction.class).forEach(x -> x.accept(this, context));
        proto.childrenOfType(ImportProduction.class).forEach(x -> x.accept(this, context));
        proto.childrenOfType(EnumProduction.class).forEach(x -> x.accept(this, context));
        proto.childrenOfType(MessageProduction.class).forEach(x -> x.accept(this, context));
        return null;
    }

    @Override
    public Void visit(PackageProduction packageProduction, GeneratorContext context) {
        context.setPackageName(packageProduction.get(1).toString());
        return null;
    }

    @Override
    public Void visit(MessageProduction message, GeneratorContext context) {
        MessagePattern messagePattern = new MessagePattern(message.get(1).toString());
        context.addMessage(messagePattern);
        message.get(2).accept(MESSAGE_VISITOR, new MessagePatternWithContext(messagePattern, context));
        return null;
    }

    @Override
    public Void visit(EnumProduction enumProduction, GeneratorContext context) {
        EnumPattern enumPattern = new EnumPattern(enumProduction.get(1).toString());
        context.addEnum(enumPattern);
        enumProduction.get(2).accept(ENUM_VISITOR, enumPattern);
        return null;
    }
}
