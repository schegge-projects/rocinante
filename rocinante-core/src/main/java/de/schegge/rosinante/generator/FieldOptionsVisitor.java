package de.schegge.rosinante.generator;

import de.schegge.rosinante.parser.ast.FieldOption;
import de.schegge.rosinante.parser.ast.FieldOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FieldOptionsVisitor<T> implements ProtoVisitor<T, Void> {
    private static final Logger logger = LoggerFactory.getLogger(FieldOptionsVisitor.class);

    public static final FieldOptionsVisitor<EnumFieldPattern> ENUM_FIELD_OPTION_VISITOR = new FieldOptionsVisitor<>();
    public static final FieldOptionsVisitor<FieldPattern> MESSAGE_FIELD_OPTION_VISITOR = new FieldOptionsVisitor<>();

    private FieldOptionsVisitor() {

    }

    @Override
    public Void visit(FieldOption fieldOption, T field) {
        String name = fieldOption.get(0).toString();
        Object constant = fieldOption.get(2).accept(ConstantVisitor.INSTANCE, null);
        logger.debug("field option: name={}, value={}", name, constant);
        return null;
    }

    @Override
    public Void visit(FieldOptions fieldOptions, T field) {
        fieldOptions.childrenOfType(FieldOption.class).forEach(x -> x.accept(this, field));
        return null;
    }
}
