package de.schegge.rosinante.generator;

import de.schegge.rosinante.parser.ast.FALSE;
import de.schegge.rosinante.parser.ast.HEX_LITERAL;
import de.schegge.rosinante.parser.ast.INTEGER_LITERAL;
import de.schegge.rosinante.parser.ast.IntegerLiteral;
import de.schegge.rosinante.parser.ast.OCT_LITERAL;
import de.schegge.rosinante.parser.ast.STRING_LITERAL;
import de.schegge.rosinante.parser.ast.TRUE;

public class ConstantVisitor implements ProtoVisitor<Void, Object> {

    public static final ConstantVisitor INSTANCE = new ConstantVisitor();

    private ConstantVisitor() {
    }

    @Override
    public Object visit(IntegerLiteral node, Void input) {
        boolean negative = "-".equals(node.get(0).toString());
        long value = (long) node.get(1).accept(this, null);
        return negative ? -value : value;
    }

    @Override
    public Object visit(STRING_LITERAL node, Void input) {
        String value = node.toString();
        return value.substring(1, value.length() - 1);
    }

    @Override
    public Object visit(TRUE node, Void input) {
        return true;
    }

    @Override
    public Object visit(FALSE node, Void input) {
        return false;
    }

    @Override
    public Object visit(INTEGER_LITERAL node, Void input) {
        return Long.parseLong(node.toString());
    }

    @Override
    public Object visit(OCT_LITERAL node, Void input) {
        return Long.parseLong(node.toString(), 8);
    }

    @Override
    public Object visit(HEX_LITERAL node, Void input) {
        return Long.parseLong(node.toString(), 16);
    }
}
