package de.schegge.rosinante.generator;

import de.schegge.rosinante.parser.Node;
import de.schegge.rosinante.parser.ast.AnyName;
import de.schegge.rosinante.parser.ast.EnumBody;
import de.schegge.rosinante.parser.ast.EnumField;
import de.schegge.rosinante.parser.ast.EnumProduction;
import de.schegge.rosinante.parser.ast.ExtensionName;
import de.schegge.rosinante.parser.ast.FALSE;
import de.schegge.rosinante.parser.ast.Field;
import de.schegge.rosinante.parser.ast.FieldOption;
import de.schegge.rosinante.parser.ast.FieldOptions;
import de.schegge.rosinante.parser.ast.HEX_LITERAL;
import de.schegge.rosinante.parser.ast.IDENTIFIER;
import de.schegge.rosinante.parser.ast.INTEGER_LITERAL;
import de.schegge.rosinante.parser.ast.ImportProduction;
import de.schegge.rosinante.parser.ast.IntegerLiteral;
import de.schegge.rosinante.parser.ast.MessageBody;
import de.schegge.rosinante.parser.ast.MessageProduction;
import de.schegge.rosinante.parser.ast.OCT_LITERAL;
import de.schegge.rosinante.parser.ast.Option;
import de.schegge.rosinante.parser.ast.PackageProduction;
import de.schegge.rosinante.parser.ast.Proto;
import de.schegge.rosinante.parser.ast.STRING_LITERAL;
import de.schegge.rosinante.parser.ast.TRUE;

public interface ProtoVisitor<I, O> {
    default O visit(Proto proto, I input) {
        return null;
    }

    default O visit(PackageProduction packageProduction, I input) {
        return null;
    }

    default O visit(ImportProduction importProduction, I input) {
        return null;
    }

    default O visit(MessageProduction message, I input) {
        return null;
    }

    default O visit(MessageBody message, I input) {
        return null;
    }

    default O visit(Field field, I input) {
        return null;
    }

    default O visit(FieldOption fieldOption, I input) {
        return null;
    }

    default O visit(FieldOptions fieldOptions, I input) {
        return null;
    }

    default O visit(Option option, I input) {
        return null;
    }

    default O visit(ExtensionName name, I input) {
        return null;
    }

    default O visit(AnyName name, I input) {
        return null;
    }

    default O visit(EnumProduction enumProduction, I input) {
        return null;
    }

    default O visit(EnumBody enumBody, I input) {
        return null;
    }

    default O visit(EnumField field, I input) {
        return null;
    }

    default O visit(IDENTIFIER node, I input) {
        return null;
    }

    default O visit(IntegerLiteral node, I input) {
        return null;
    }

    default O visit(STRING_LITERAL node, I input) {
        return null;
    }

    default O visit(TRUE node, I input) {
        return null;
    }

    default O visit(FALSE node, I input) {
        return null;
    }

    default O visit(INTEGER_LITERAL node, I input) {
        return null;
    }

    default O visit(OCT_LITERAL node, I input) {
        return null;
    }

    default O visit(HEX_LITERAL node, I input) {
        return null;
    }

    default O visit(Node node, I input) {
        return null;
    }
}
