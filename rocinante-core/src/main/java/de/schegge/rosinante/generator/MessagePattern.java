package de.schegge.rosinante.generator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MessagePattern {
    private final String name;
    private final Map<String, FieldPattern> fields = new LinkedHashMap<>();

    public MessagePattern(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<FieldPattern> getFields() {
        return List.copyOf(fields.values());
    }

    public void addField(FieldPattern fieldPattern) {
        checkForDuplicates(fieldPattern);
        fields.put(fieldPattern.name(), fieldPattern);
    }

    private void checkForDuplicates(FieldPattern pattern) {
        List<String> existingFieldNames = fields.values().stream().map(FieldPattern::name).filter(x -> pattern.name().equals(x)).toList();
        if (!existingFieldNames.isEmpty()) {
            throw new IllegalArgumentException("duplicate field name: " + existingFieldNames);
        }
    }

    public long getMandatoryFields() {
        return fields.values().stream().filter(f -> !f.optional()).count();
    }
}
