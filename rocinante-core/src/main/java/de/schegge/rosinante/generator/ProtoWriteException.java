package de.schegge.rosinante.generator;

public class ProtoWriteException extends RuntimeException {
    public ProtoWriteException(Throwable e) {
        super(e);
    }
}
