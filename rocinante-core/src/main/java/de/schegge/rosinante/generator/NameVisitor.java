package de.schegge.rosinante.generator;

import de.schegge.rosinante.parser.ast.AnyName;
import de.schegge.rosinante.parser.ast.ExtensionName;
import de.schegge.rosinante.parser.ast.IDENTIFIER;

public class NameVisitor implements ProtoVisitor<Void, String> {
    @Override
    public String visit(ExtensionName name, Void input) {
        return name.toString();
    }

    @Override
    public String visit(AnyName name, Void input) {
        return name.toString();
    }

    @Override
    public String visit(IDENTIFIER name, Void input) {
        return name.toString();
    }
}
