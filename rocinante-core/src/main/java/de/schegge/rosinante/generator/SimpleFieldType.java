package de.schegge.rosinante.generator;

public record SimpleFieldType(String type, String wrapper, String io, String initial, int complex, boolean packable) implements FieldType {
    SimpleFieldType(String type, String wrapper, String io, String initial) {
        this(type, wrapper,io, initial, 0, true);
    }
}
