package de.schegge.rosinante.generator;

public interface FieldType {
    String type();

    String wrapper();

    String initial();

    String io();

    default int complex() {
        return 0;
    }

    default boolean packable() {
        return false;
    }
}
