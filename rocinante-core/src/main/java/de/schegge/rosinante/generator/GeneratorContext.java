package de.schegge.rosinante.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class GeneratorContext {

    private static final Map<String, FieldType> TYPES = Map.ofEntries(
            Map.entry("string", new SimpleFieldType("String", "String", "String", "\"\"", 0, false)),
            Map.entry("bytes", new SimpleFieldType("byte[]", "byte[]", "Bytes", "new byte[0]", 0, false)),
            Map.entry("bool", new SimpleFieldType("boolean", "Boolean", "Boolean", "false")),
            Map.entry("int32", new SimpleFieldType("int", "Integer", "Integer", "0")),
            Map.entry("int64", new SimpleFieldType("long", "Long", "Long", "0L")),
            Map.entry("sint32", new SimpleFieldType("int", "Integer", "ZigZagInteger", "0")),
            Map.entry("sint64", new SimpleFieldType("long", "Long", "ZigZagLong", "0L"))
    );

    private String packageName;
    private final List<MessagePattern> messages = new ArrayList<>();
    private final List<EnumPattern> enums = new ArrayList<>();

    private final Set<String> types = new HashSet<>();

    public void setPackageName(String packageName) {
        this.packageName = Objects.requireNonNull(packageName);
    }

    public String getPackageName() {
        return packageName;
    }

    public void addMessage(MessagePattern messagePattern) {
        if (types.contains(messagePattern.getName())) {
            throw new IllegalArgumentException("duplicate name: " + messagePattern.getName());
        }
        messages.add(messagePattern);
    }

    public void addEnum(EnumPattern enumPattern) {
        if (types.contains(enumPattern.getName())) {
            throw new IllegalArgumentException("duplicate name: " + enumPattern.getName());
        }
        enums.add(enumPattern);
    }

    public Stream<MessagePattern> getMessages() {
        return messages.stream();
    }

    public Stream<EnumPattern> getEnums() {
        return enums.stream();
    }

    public FieldType getType(String typeName) {
        FieldType type = TYPES.get(typeName);
        if (type != null) {
            return type;
        }
        if (getMessages().anyMatch(m -> typeName.equals(m.getName()))) {
            return new SimpleFieldType(typeName, typeName, null, "null", 1, false);
        } else if (getEnums().anyMatch(m -> typeName.equals(m.getName()))) {
            return new SimpleFieldType(typeName, typeName, null, "null", 2, false);
        }
        throw new IllegalArgumentException("no type found: " + typeName);
    }
}
