package de.schegge.rosinante.generator;

public record FieldPattern(FieldType type, String name, int fieldNumber, boolean optional, boolean repeated) {
    FieldPattern(String name, FieldType type, int fieldNumber, boolean optional, boolean repeated) {
        this(type, name, fieldNumber, optional, repeated);
    }
}
