package de.schegge.rosinante.generator;

import java.util.List;

public record EnumFieldPattern(List<String> names, int fieldNumber) {
}
