package de.schegge.rosinante.core;

import java.io.IOException;

public interface ProtoSources {
    String getContent(String name) throws IOException;

    String getImport(String name) throws IOException;
}
