package de.schegge.rosinante.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PathProtoSources implements ProtoSources {
    private final Path importPath;
    private final Path inputPath;

    public PathProtoSources(Path inputPath, Path importPath) {
        this.inputPath = inputPath;
        this.importPath = importPath;
    }

    @Override
    public String getContent(String name) throws IOException {
        return Files.readString(inputPath.resolve(name));
    }

    @Override
    public String getImport(String name) throws IOException {
        return Files.readString(importPath.resolve(name));
    }
}
