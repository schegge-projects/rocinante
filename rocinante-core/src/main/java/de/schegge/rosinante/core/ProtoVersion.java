package de.schegge.rosinante.core;

public enum ProtoVersion {
    PROTO2,
    PROTO3,
    ROSI1
}
