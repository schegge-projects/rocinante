package de.schegge.rosinante.core;

import java.io.IOException;

public interface ProtoDestinations {
    void writeFile(String packageName, String fileName, String content) throws IOException;
}
