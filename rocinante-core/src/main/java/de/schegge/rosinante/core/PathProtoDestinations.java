package de.schegge.rosinante.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;


public class PathProtoDestinations implements ProtoDestinations {
    private static final Logger logger = LoggerFactory.getLogger(PathProtoDestinations.class);
    private final Path targetPath;

    public PathProtoDestinations(Path targetPath) {
        this.targetPath = targetPath;
    }

    @Override
    public void writeFile(String packageName, String fileName, String content) throws IOException {
        logger.debug("write file: package-name={}, file-name: {}", packageName, fileName);
        Path resolved = targetPath;
        Optional<String> packagePath = Optional.ofNullable(packageName).map(x -> x.replace('.', '/'));
        if (packagePath.isPresent()) {
            resolved = targetPath.resolve(packagePath.get());
            Files.createDirectories(resolved);
        }
        Files.writeString(resolved.resolve(fileName), content);
    }
}
