= Rocinante
:icons: font

A poor man protocol buffer as described in the blog article https://schegge.de/2024/03/rocinante-a-poor-mans-protocol-buffer/[Rocinante – a poor mans protocol buffer]

The feature list of _Rocinante_ is a subset of _Protocol Buffer 3_. There is compatibility at the protocol level in the coding, but there will be no compatibility with the Java API of Protocol Buffer.

|===
2+|Feature | Supported | Comment

.6+|Wire-Type
| VARINT | yes |
| LEN | yes |
| I32 | |
| I64 | |
| SGROUP | |
| EGROUP | |
.16+|Scalar type
| int32 | yes | negative values a encoded as 5 bytes, not 10
| int64 | yes |
| uint32 | |
| uint64 | |
| sint32 | yes |
| sint64 | yes |
| bool  | yes |
| enum | yes | no real open or closed semantics. unknown values are ignored
, reserved values are still missing
| float | |
| double | |
| fixed32 | |
| fixed64 | |
| sfixed32 | |
| sfixed64 | |
| string | yes |
| bytes | yes |
2+| Messages | yes |
2+| Maps | |
2+| Nested Types | |
|===


