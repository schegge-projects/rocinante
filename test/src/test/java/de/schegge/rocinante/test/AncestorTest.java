package de.schegge.rocinante.test;

import de.schegge.ancestor.Ancestor;
import de.schegge.ancestor.AncestorMessage;
import de.schegge.ancestor.AncestorMessageProto;
import de.schegge.ancestor.AncestorMessageType;
import de.schegge.ancestor.AncestorProto;
import de.schegge.example.Example;
import de.schegge.example.ExampleProto;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HexFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AncestorTest {
    @Test
    void test() throws IOException {
        Ancestor ancestor = new AncestorProto().builder().setFirstname("Jens").setLastname("Kaiser").build();
        AncestorMessageProto ancestorMessageProto = new AncestorMessageProto();
        AncestorMessage ancestorMessage1 = ancestorMessageProto.builder().setAncestor(ancestor).setType(AncestorMessageType.UPDATE).build();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ancestorMessageProto.write(outputStream, ancestorMessage1);
        HexFormat hexFormat = HexFormat.ofDelimiter(" ");
        byte[] byteArray = outputStream.toByteArray();
        assertEquals("08 01 12 0e 0a 06 4b 61 69 73 65 72 12 04 4a 65 6e 73", hexFormat.formatHex(byteArray));
        AncestorMessage ancestorMessage2 = ancestorMessageProto.read(new ByteArrayInputStream(byteArray));
        assertAll(
                () -> assertEquals("Jens", ancestorMessage2.ancestor().firstname()),
                () -> assertEquals("Kaiser", ancestorMessage2.ancestor().lastname()),
                () -> assertEquals(AncestorMessageType.UPDATE, ancestorMessage2.type())
        );
    }

    @Test
    void test2() throws IOException {
        ExampleProto exampleProto = new ExampleProto();
        Example ancestorMessage1 = exampleProto.builder().setText("Rocinante").setFlag(true).setNumber(42).setNumbers(List.of(42))
                .setNames(List.of("name1", "name2")).build();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        exampleProto.write(outputStream, ancestorMessage1);
        HexFormat hexFormat = HexFormat.ofDelimiter(" ");
        byte[] byteArray = outputStream.toByteArray();
        assertEquals("0a 09 52 6f 63 69 6e 61 6e 74 65 10 01 18 2a 22 01 2a 2a 05 6e 61 6d 65 31 2a 05 6e 61 6d 65 32", hexFormat.formatHex(byteArray));
    }
}
