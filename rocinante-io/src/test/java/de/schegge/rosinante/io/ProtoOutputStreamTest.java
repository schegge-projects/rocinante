package de.schegge.rosinante.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HexFormat;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ProtoOutputStreamTest {
    @Test
    void writeBoolean() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeBoolean(1, true);
        protoOutputStream.writeBoolean(2, false);
        assertArrayEquals(new byte[]{8, 1, 16, 0}, byteArrayOutputStream.toByteArray());
    }

    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffff0f"
    })
    void writeInteger(int value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeInteger(1, value);
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,08ac02",
            "278,08ac04",
            "-2,0803",
            "-64,087f",
            "-65,088101",
            "63,087e",
            "64,088001",
    })
    void writeZigZagInteger(int value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeZigZagInteger(1, value);
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffffffffffffff01"
    })
    void writeLong(long value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeLong(1, value);
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,08ac02",
            "278,08ac04",
            "-2,0803"
    })
    void writeZigZagLong(long value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeZigZagLong(1, value);
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @Test
    void writeString() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeString(1, "test");
        assertArrayEquals(new byte[]{10, 4, 116, 101, 115, 116}, byteArrayOutputStream.toByteArray());
    }

    @Test
    void writeBytes() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeBytes(1, new byte[]{116, 101, 115, 116});
        assertArrayEquals(new byte[]{10, 4, 116, 101, 115, 116}, byteArrayOutputStream.toByteArray());
    }
}