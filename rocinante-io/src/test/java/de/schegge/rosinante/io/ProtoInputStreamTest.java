package de.schegge.rosinante.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HexFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProtoInputStreamTest {
    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffff0f"
    })
    void readInteger(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        int protoInteger = protoInputStream.readInteger(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(value, protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffffffffffffff01"
    })
    void readLong(long value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        long protoLong = protoInputStream.readLong(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(value, protoLong);
    }

    @ParameterizedTest
    @CsvSource({
            "150,08ac02",
            "278,08ac04",
            "-2,0803"
    })
    void readZigZagInteger(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        int protoInteger = protoInputStream.readZigZagInteger(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(value, protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,08ac02",
            "278,08ac04",
            "-2,0803"
    })
    void readZigZagLong(long value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        long protoInteger = protoInputStream.readZigZagLong(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(value, protoInteger);
    }

    @Test
    void readBoolean() throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{8, 1, 16, 0});
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        boolean protoBoolean = protoInputStream.readBoolean(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertTrue(protoBoolean);
        wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        protoBoolean = protoInputStream.readBoolean(wireType);
        assertEquals(2, protoInputStream.getLastFieldNumber());
        assertFalse(protoBoolean);
    }

    @Test
    void readString() throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{10, 4, 116, 101, 115, 116});
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType);
        String protoString = protoInputStream.readString(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals("test", protoString);
    }

    @Test
    void readBytes() throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{10, 4, 116, 101, 115, 116});
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType);
        String protoString = new String(protoInputStream.readBytes(wireType));
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals("test", protoString);
    }
}