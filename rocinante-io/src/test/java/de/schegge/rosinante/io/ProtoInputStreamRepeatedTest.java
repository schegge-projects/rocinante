package de.schegge.rosinante.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HexFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProtoInputStreamRepeatedTest {
    @ParameterizedTest
    @CsvSource({
            "150,0a0496019601",
            "278,0a0496029602",
            "-2,0a0afeffffff0ffeffffff0f"
    })
    void readRepeatedIntegers(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType);
        List<Integer> protoInteger = protoInputStream.readIntegers(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value, value), protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffff0f"
    })
    void readIntegers(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        List<Integer> protoInteger = protoInputStream.readIntegers(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value), protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a04ac02ac02",
            "278,0a04ac04ac04",
            "-2,0a020303",
            "-64,0a027f7f",
            "-65,0a0481018101",
            "63,0a027e7e",
            "64,0a0480018001",
    })
    void readRepeatedZigZagIntegers(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType);
        List<Integer> protoInteger = protoInputStream.readZigZagIntegers(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value, value), protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,08ac02",
            "278,08ac04",
            "-2,0803"
    })
    void readZigZagIntegers(int value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        List<Integer> protoInteger = protoInputStream.readZigZagIntegers(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value), protoInteger);
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a0496019601",
            "278,0a0496029602",
            "-2,0a14feffffffffffffffff01feffffffffffffffff01"
    })
    void readRepeatedLongs(long value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType);
        List<Long> protoLong = protoInputStream.readLongs(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value, value), protoLong);
    }

    @ParameterizedTest
    @CsvSource({
            "150,089601",
            "278,089602",
            "-2,08feffffffffffffffff01"
    })
    void readLongs(long value, String hex) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(HexFormat.of().parseHex(hex));
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType = protoInputStream.readType();
        assertEquals(WireType.VARINT, wireType);
        List<Long> protoLong = protoInputStream.readLongs(wireType);
        assertEquals(1, protoInputStream.getLastFieldNumber());
        assertEquals(List.of(value), protoLong);
    }

    @Test
    void readBoolean() throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{10, 2, 1, 0, 18, 2, 0, 1});
        ProtoInputStream protoInputStream = new ProtoInputStream(byteArrayInputStream);
        WireType wireType1 = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType1);
        assertEquals(List.of(true, false), protoInputStream.readBooleans(wireType1));
        WireType wireType2 = protoInputStream.readType();
        assertEquals(WireType.LEN, wireType2);
        assertEquals(List.of(false, true), protoInputStream.readBooleans(wireType2));
    }
}