package de.schegge.rosinante.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HexFormat;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ProtoOutputStreamRepeatedTest {
    @Test
    void writeBoolean() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeBoolean(1, List.of(true, false));
        protoOutputStream.writeBoolean(2, List.of(false, true));
        assertArrayEquals(new byte[]{10, 2, 1, 0, 18, 2, 0, 1}, byteArrayOutputStream.toByteArray());
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a0496019601",
            "278,0a0496029602",
            "-2,0a0afeffffff0ffeffffff0f"
    })
    void writeInteger(int value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeInteger(1, List.of(value, value));
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a04ac02ac02",
            "278,0a04ac04ac04",
            "-2,0a020303",
            "-64,0a027f7f",
            "-65,0a0481018101",
            "63,0a027e7e",
            "64,0a0480018001",
    })
    void writeZigZagInteger(int value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeZigZagInteger(1, List.of(value, value));
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a0496019601",
            "278,0a0496029602",
            "-2,0a14feffffffffffffffff01feffffffffffffffff01"
    })
    void writeLong(long value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeLong(1, List.of(value, value));
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @ParameterizedTest
    @CsvSource({
            "150,0a04ac02ac02",
            "278,0a04ac04ac04",
            "-2,0a020303"
    })
    void writeZigZagLong(long value, String hex) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeZigZagLong(1, List.of(value, value));
        assertEquals(hex, HexFormat.of().formatHex(byteArrayOutputStream.toByteArray()));
    }

    @Test
    void writeString() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeString(1, List.of("test1", "test2"));
        assertArrayEquals(new byte[]{10, 5, 116, 101, 115, 116, 49, 10, 5, 116, 101, 115, 116, 50}, byteArrayOutputStream.toByteArray());
    }

    @Test
    void writeBytes() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ProtoOutputStream protoOutputStream = new ProtoOutputStream(byteArrayOutputStream);

        protoOutputStream.writeBytes(1, List.of(new byte[]{116, 101, 115, 116, 49}, new byte[]{116, 101, 115, 116, 50}));
        assertArrayEquals(new byte[]{10, 5, 116, 101, 115, 116, 49, 10, 5, 116, 101, 115, 116, 50}, byteArrayOutputStream.toByteArray());
    }
}