package de.schegge.rosinante.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HexFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LimitedProtoInputStreamTest {

    @Test
    void read() throws IOException {
        LimitedProtoInputStream limitedProtoInputStream = new LimitedProtoInputStream(new ByteArrayInputStream(HexFormat.of().parseHex("96019601")), 2);
        assertEquals(150, limitedProtoInputStream.readVariableByteInt());
        assertEquals(0, limitedProtoInputStream.available());
        assertThrows(IOException.class, limitedProtoInputStream::readVariableByteInt);
    }
}