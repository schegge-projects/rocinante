package de.schegge.rosinante.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Proto<T> {
    T read(InputStream inputStream) throws IOException;

    void write(OutputStream outputStream, T value) throws IOException;

    Builder<T> builder();
}
