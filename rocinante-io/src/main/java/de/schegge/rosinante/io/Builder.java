package de.schegge.rosinante.io;

public interface Builder<T> {
    T build();
}
