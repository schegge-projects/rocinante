package de.schegge.rosinante.io;

import java.io.IOException;
import java.io.InputStream;

public class LimitedProtoInputStream extends ProtoInputStream {
    private final int limit;
    private int count;

    public LimitedProtoInputStream(InputStream in, int limit) {
        super(in);
        this.limit = limit;
    }


    @Override
    public int read() throws IOException {
        return count++ < limit ? super.read() : -1;
    }

    @Override
    public int available() {
        return limit - count;
    }

    @Override
    public void close() {

    }


}
