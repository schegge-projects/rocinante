package de.schegge.rosinante.io;

public enum WireType {
    VARINT, I64, LEN, SGROUP, EGROUP, I32
}
